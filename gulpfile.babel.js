import gulp from 'gulp';
import autoprefixer from 'gulp-autoprefixer';
import concat from 'gulp-concat';
import less from 'gulp-less';
import cssnano from 'gulp-cssnano';
import browserSync from "browser-sync";
import imagemin from 'gulp-imagemin';

const reload = browserSync.reload;

const path = {
	build: {
		html: './build/',
		css: './build/app/css/',
		img: './build/app/images/',
		fonts: './build/app/fonts/'
	},
	src: {
		html: './src/**/*.html',
		styles: './src/app/styles/index.less',
		img: './src/app/images/**/*.*',
		fonts: './src/app/fonts/**/*.*'

	},
	watch: {
		html: './src/**/*.html',
		styles: './src/app/styles/*.less',
		img: './src/app/img/**/*.*',
		fonts: './src/app/fonts/**/*.*'
	}
};

gulp.task('webserver', () => {
	browserSync({
		server: {
			baseDir: "./build"
		},
		tunnel: true,
		host: 'localhost',
		port: 9000
	})
});

gulp.task('styles:build', () => {
	return gulp.src(path.src.styles)
		.pipe(less())
		.pipe(autoprefixer())
		.pipe(cssnano())
		.pipe(concat('index.css'))
		.pipe(gulp.dest(path.build.css))
		.pipe(reload({stream: true}))
		;
});

gulp.task('html:build', () => {
	return gulp.src(path.src.html)
		.pipe(gulp.dest(path.build.html))
		.pipe(reload({stream: true}))
		;
});

gulp.task('fonts:build', () => {
	return gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts))
		.pipe(reload({stream: true}))
		;
});

gulp.task('images:build', () => {
	return gulp.src(path.src.img)
		.pipe(imagemin())
		.pipe(gulp.dest(path.build.img))
		.pipe(reload({stream: true}))
		;
});

gulp.task('build', [
	'styles:build',
	'html:build',
	'fonts:build',
	'images:build'
]);

gulp.task('default', [
	'build',
	'watch'
]);

gulp.task('watch', () => {
	gulp.watch(path.watch.styles, ['styles:build']),
	gulp.watch(path.watch.html,   ['html:build'])
});

